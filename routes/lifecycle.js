let express = require("express");
let router = express.Router();
const fs = require('fs');
var messages = require('../helper/messages');
const _ = require('lodash');

/**
 * Handle the installed event.
 */

router.post('/installed',
  function (req, res) {
    console.log('App installed in a conversation.');
    cloudId = req.body.cloudId;
    conversationId = req.body.resourceId;
    const message = {
  
          version: 1,
          type: "doc",
          content: [
            {
              type: "paragraph",
              content: [
                {
                  type: "text",
                  text: "Hi there! Thanks for adding me to this conversation. To see me in action, just mention me in a message."
                }
              ]
            }
          ]
    }
    messages.sendMessage(cloudId, conversationId, message, function (err, response) {
      if (err)
        console.log(err);
    });
    res.sendStatus(204);
  }
);

/**
 * Handle the uninstalled event
 * This doesn't do anything for our example.
 */

router.post('/uninstalled', (req, res) => {
  console.log('App uninstalled from a conversation.');
  res.sendStatus(204);
});

/**
 * Handle any request for your descriptor.
 */
router.get('/descriptor', function (req, res) {
  fs.readFile('./app-descriptor.json', function (err, descriptorTemplate) {
    const template = _.template(descriptorTemplate);
    const descriptor = template({
      host: 'https://' + req.headers.host
    });
    res.set('Content-Type', 'application/json');
    res.send(descriptor);
  });
});

module.exports = router;