let express = require("express");
let router = express.Router();

const jwtUtil = require('jwt-simple');

var { Document } = require('adf-builder');

var messages = require('../helper/messages');

/**
 * Return a simple application card.
 */
router.get('/',
function(req, res){
  const encodedJWT = req.headers["authorization"].substring(7); 
  const jwt = jwtUtil.decode(encodedJWT, null, true);
  const cloudId = jwt.context.cloudId;
  const conversationId = jwt.context.resourceId;
  var doc = new Document();

  doc.applicationCard('Charlie updated a file: applicationCard.md')
    .description('Charlie updated a file: applicationCard.md');

  /**
   * Let's use JSON.stringify to get a nicely formatted JSON version of our example.
   */
  var code = JSON.stringify(doc, null, 4);

  /**
   * Now we'll reset our doc and create the message showing all the information.
   */
  doc = new Document();
  doc.rule();
  doc.heading(3)
    .text("Simple application card example:");
  doc.rule();
  doc.applicationCard('Charlie updated a file: applicationCard.md')
    .description('Charlie updated a file: applicationCard.md');
  doc.rule();
  doc.paragraph()
    .text('Here is the JavaScript code to build the example above:');
  doc.codeBlock("javascript")
    .text('var { Document } = require(\'adf-builder\'); \
    \n doc.applicationCard(\'Charlie updated a file: applicationCard.md\') \
    \n    .description(\'Charlie updated a file: applicationCard.md\'); \
    ');
  doc.paragraph()
    .text('... and here is the example message in JSON (ADF):');
  doc.codeBlock("javascript")
    .text(code);
  
  var reply = doc.toJSON();
  messages.sendMessage(cloudId, conversationId, reply, function (err, response) {
    if (err)
    console.log(err);
  });
  res.sendStatus(204);
});

module.exports = router;