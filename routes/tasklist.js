let express = require("express");
let router = express.Router();

const jwtUtil = require('jwt-simple');

var messages = require('../helper/messages');

/**
 * Return a task list.
 */
router.get('/',
function(req, res){
  const encodedJWT = req.headers["authorization"].substring(7); 
  const jwt = jwtUtil.decode(encodedJWT, null, true);
  const cloudId = jwt.context.cloudId;
  const conversationId = jwt.context.resourceId;
  /**
   * There is currently no ADF client library support for task list.
   * So we are showing only the ADF (JSON) code.
   */

  var code = "{\r\n    \"type\": \"doc\",\r\n    \"content\": [ {\r\n        \"type\": \"taskList\",\r\n        \"attrs\": {\r\n          \"localId\": \"test-list-id\"\r\n        },\r\n        \"content\": [\r\n          {\r\n            \"type\": \"taskItem\",\r\n            \"attrs\": {\r\n              \"localId\": \"test-id-1\",\r\n              \"state\": \"TODO\"\r\n            },\r\n            \"content\": [\r\n              {\r\n                \"type\": \"text\",\r\n                \"text\": \"Do this!\"\r\n              }\r\n            ]\r\n          },\r\n          {\r\n            \"type\": \"taskItem\",\r\n            \"attrs\": {\r\n              \"localId\": \"test-id-2\",\r\n              \"state\": \"DONE\"\r\n            },\r\n            \"content\": [\r\n              {\r\n                \"type\": \"text\",\r\n                \"text\": \"This is completed\"\r\n              }\r\n            ]\r\n          }\r\n        ]\r\n      }\r\n    ],\r\n    \"version\": 1\r\n  }";

  var reply = {
    "type": "doc",
    "content": [ 
      {
        "type": "rule"
      },
      {
        "type": "heading",
        "attrs":{
          "level": 3
        },
        "content": [
          {
            "type": "text",
            "text": "Task list example: "
          }
        ]
      },
      {
        "type": "rule"
      },
      {
        "type": "taskList",
        "attrs": {
          "localId": "test-list-id"
        },
        "content": [
          {
            "type": "taskItem",
            "attrs": {
              "localId": "test-id-1",
              "state": "TODO"
            },
            "content": [
              {
                "type": "text",
                "text": "Do this!"
              }
            ]
          },
          {
            "type": "taskItem",
            "attrs": {
              "localId": "test-id-2",
              "state": "DONE"
            },
            "content": [
              {
                "type": "text",
                "text": "This is completed"
              }
            ]
          }
        ]
      },
      {
        "type": "rule"
      },
      {
        "type": "paragraph",
        "content":[
          {
            "type": "text",
            "text": "Here is the JSON (ADF) code to build the example above. \nNOTE: Tasklist is currently not supported by adf-builder."
          }
        ]
      },
      {
        "type": "codeBlock",
        "attrs": {
          "language": "javascript"
        },
        "content": [
          {
            "type": "text",
            "text": code
          }
        ]
      }
    ],
    "version": 1
  }

  messages.sendMessage(cloudId, conversationId, reply, function (err, response) {
    if (err)
    console.log(err);
  });
  res.sendStatus(204)
});

module.exports = router;